package com.devs.appdeporte.estructura;

import androidx.appcompat.app.AlertDialog;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

public class Variables {

    public static final int ID_HOME = 1;
    public static final int ID_MESSAGE = 2;
    public static final int ID_NOTIFICATION = 3;
    public static final int ID_ACOUNT = 4;
    public static final int ID_SPOT = 5;


    public static  final int PAYPAL_REQUEST_CODE = 7171;

    public static final int REQUEST_CALL = 1;

    public static final int idUnica = 51623;

    public static int PICK_IMAGE_REQUEST = 1;

    public static String localizacion = "", latitud = "", longitud = "";

    public static LatLng latLng;

    public static String
            code_paypal = "", q_s = "1", modal = "", instagran = "https://www.instagram.com/?hl=es-la", facebook = "https://www.facebook.com/", whatsapp = "https://wa.me/5217775074965", favoritos = "0", url = "", id_principal = "0", visitado = "0", compra_paypal = "", productos = "", id_venta = "0", id = "0",amount = "10", item_tipo = "", nombre = "", apellido = "", email = "", password = "", repetir = "", conjunto = "", apto = "", interior = "", sexo = "", numero = "",
            vehiculo = "0", imagen = "", mascota = "0", opcion = "0", error_login = "0",message = "", CATEGORIAS = "1", CATEGORIA_SERIES = "0", CATEGORIA_CINE = "0", CATEGORIA_CANAL = "0",
            categoria = "2", id_servicios = "0", item_comentario = "", idioma = "0", temporada = "1", img_serie = "",
            error = "0", permisos = "0", estado_search = "0", estado_menu = "0", consola = "0", item_id = "", item_cantidad = "0", item_total = "0",
            id_item = "0", item_fondo = "", item_genero = "", item_titulo = "", item_titulo_ingles = " ", item_fecha = "", item_contenido = "", item_contenido_ingles = "", item_img = "", item_trailer = "", item_precio,
            day, month, year, hoy, item, telefono = "+573174233892", direccion = "",
            mensaje_campos = "Debe llenar todos los campos.", mensaje = "", mensaje_fallido = "Error en el ingreso.", mensaje_exitoso = "Agregada exitosamente", mensaje_exitoso_actualizar = "Datos Actualizados."
            ;

    public static int
            dia = 0,mes = 0, ano = 0000, hora = 00, minutos = 00, tiempo = 5000 , valor_noticia = 0;

    public static ArrayList<String> array_deporte_id = new ArrayList<String>();
    public static ArrayList<String> array_deporte_nombre = new ArrayList<String>();

    public static ArrayList<String> array_dificultad_id = new ArrayList<String>();
    public static ArrayList<String> array_dificultad_nombre = new ArrayList<String>();

    public static ArrayList<String> array_actividad_id = new ArrayList<String>();
    public static ArrayList<String> array_actividad_nombre = new ArrayList<String>();

    public static ArrayList<String> array_duracion_id = new ArrayList<String>();
    public static ArrayList<String> array_duracion_nombre = new ArrayList<String>();

    //Array Productos

    public static ArrayList<String> array_productos_id = new ArrayList<String>();

    public static ArrayList<String> array_productos_titulo = new ArrayList<String>();

    public static ArrayList<String> array_productos_precio = new ArrayList<String>();

    public static ArrayList<String> array_productos_img = new ArrayList<String>();

    //Array Mensajes

    public static ArrayList<String> array_mensajes_id = new ArrayList<String>();

    public static ArrayList<String> array_mensajes_titulo = new ArrayList<String>();

    public static ArrayList<String> array_mensajes_precio = new ArrayList<String>();

    public static ArrayList<String> array_mensajes_img = new ArrayList<String>();

}
