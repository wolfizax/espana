package com.devs.appdeporte.estructura;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.devs.appdeporte.R;


public class Modals {

    Funciones funciones = new Funciones();
    Variables variables = new Variables();
    Objetos objetos = new Objetos();

    public void modal_crear_quedadas(Activity activity, String string) {

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(activity);
        View mView = activity.getLayoutInflater().inflate(R.layout.modal_post, null);

        Button guardar = mView.findViewById(R.id.btn_guardar);
        TextView fecha = mView.findViewById(R.id.lbl_fecha);
        EditText link = mView.findViewById(R.id.txt_link);
        ImageView img_calendario = mView.findViewById(R.id.img_calendario);

        fecha.setText(string);

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();

        img_calendario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                funciones.calendario(fecha, activity);
            }
        });

        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                dialog.dismiss();

            }
        });

        if(dialog.getWindow() != null){
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        }

        dialog.show();

    }

    public void modal_select_(Activity activity, String string) {

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(activity);
        View mView = activity.getLayoutInflater().inflate(R.layout.modal_post, null);

        RecyclerView rv = mView.findViewById(R.id.btn_guardar);


        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();

        if(dialog.getWindow() != null){
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        }

        dialog.show();

    }

    public void modal_select_actividad(Activity activity, String string) {

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(activity);
        View mView = activity.getLayoutInflater().inflate(R.layout.modal_actividad, null);

        RecyclerView rv = mView.findViewById(R.id.rv);
        objetos.lbl_guardar = mView.findViewById(R.id.lbl_guardar);
        ConstraintLayout guardar = mView.findViewById(R.id.c_guardar);

        funciones.cargarvalores(activity, rv, variables.array_actividad_nombre, "3");

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();

        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        if(dialog.getWindow() != null){
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        }

        dialog.show();

    }

    public void modal_select_deporte(Activity activity, String string) {

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(activity);
        View mView = activity.getLayoutInflater().inflate(R.layout.modal_deportes, null);

        RecyclerView rv = mView.findViewById(R.id.rv);
        objetos.lbl_guardar = mView.findViewById(R.id.lbl_guardar);
        ConstraintLayout guardar = mView.findViewById(R.id.c_guardar);

        funciones.cargarvalores(activity, rv, variables.array_deporte_nombre, "1");

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();

        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        if(dialog.getWindow() != null){
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        }

        dialog.show();

    }

    public void modal_select_dificultad(Activity activity, String string) {

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(activity);
        View mView = activity.getLayoutInflater().inflate(R.layout.modal_dificultad, null);

        RecyclerView rv = mView.findViewById(R.id.rv);
        objetos.lbl_guardar = mView.findViewById(R.id.lbl_guardar);
        ConstraintLayout guardar = mView.findViewById(R.id.c_guardar);

        funciones.cargarvalores(activity, rv, variables.array_dificultad_nombre, "2");

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();

        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        if(dialog.getWindow() != null){
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        }

        dialog.show();

    }

    public void modal_select_duracion(Activity activity, String string) {

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(activity);
        View mView = activity.getLayoutInflater().inflate(R.layout.modal_dificultad, null);

        RecyclerView rv = mView.findViewById(R.id.rv);
        objetos.lbl_guardar = mView.findViewById(R.id.lbl_guardar);
        ConstraintLayout guardar = mView.findViewById(R.id.c_guardar);

        funciones.cargarvalores(activity, rv, variables.array_duracion_nombre, "4");

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();

        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        if(dialog.getWindow() != null){
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        }

        dialog.show();

    }

    public void modal_descripcion(Activity activity, String string) {

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(activity);
        View mView = activity.getLayoutInflater().inflate(R.layout.modal_descripcion, null);

        RecyclerView rv = mView.findViewById(R.id.rv);
        objetos.txt_descripcion = mView.findViewById(R.id.txt_descrpcion);
        objetos.lbl_guardar = mView.findViewById(R.id.lbl_guardar);
        ConstraintLayout guardar = mView.findViewById(R.id.c_guardar);

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();

        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                objetos.lbl_descripcion.setText(objetos.txt_descripcion.getText().toString());
                dialog.dismiss();
            }
        });

        if(dialog.getWindow() != null){
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        }

        dialog.show();

    }

    public void modal_select_spot(Activity activity, String string) {

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(activity);
        View mView = activity.getLayoutInflater().inflate(R.layout.modal_spot, null);

        RecyclerView rv = mView.findViewById(R.id.rv);


        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();

        if(dialog.getWindow() != null){
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        }

        dialog.show();

    }


}
