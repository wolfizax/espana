package com.devs.appdeporte.controller;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import com.devs.appdeporte.R;
import com.devs.appdeporte.estructura.Funciones;
import com.devs.appdeporte.estructura.Objetos;
import com.devs.appdeporte.estructura.Variables;

public class Producto extends AppCompatActivity {

    Variables v = new Variables();
    Funciones funciones = new Funciones();
    Objetos o = new Objetos();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_producto);

        OBJETOS();
        ACCIONES();

    }

    private void OBJETOS(){
        o.img_back = findViewById(R.id.img_back);
    }

    private void ACCIONES(){
        o.img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                funciones.go(Producto.this, Home.class);
            }
        });
    }

}