package com.devs.appdeporte.controller;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.devs.appdeporte.R;
import com.devs.appdeporte.estructura.Objetos;

public class Login extends AppCompatActivity {

    Objetos o = new Objetos();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        OBJETOS();

        ACCIONES();

    }

    private void OBJETOS(){

        o.lbl_registro = findViewById(R.id.lbl_registro);
        o.btn_login = findViewById(R.id.btn_login);

    }

    private void ACCIONES(){

        o.lbl_registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Login.this , Registro.class);

                startActivity(intent);
            }
        });

        o.btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Login.this , Home.class);

                startActivity(intent);
            }
        });
    }
}