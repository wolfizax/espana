package com.devs.appdeporte.items;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.devs.appdeporte.R;
import com.devs.appdeporte.estructura.Funciones;
import com.devs.appdeporte.estructura.Link;
import com.devs.appdeporte.estructura.Objetos;
import com.devs.appdeporte.estructura.Variables;

import java.util.ArrayList;

/**
 * Created by Aws on 28/01/2018.
 */

public class AdapterNotificaciones extends RecyclerView.Adapter<AdapterNotificaciones.MyViewHolder> {

    Variables variables = new Variables();

    Objetos objetos = new Objetos();

    Link link = new Link();

    Funciones funciones = new Funciones();

    boolean ischeckDone = false;

    private Context mContext ;
    ArrayList<String> titulo = new ArrayList<String>();
    ArrayList<String> img = new ArrayList<String>();
    ArrayList<String> detalle = new ArrayList<String>();
    ArrayList<String> id = new ArrayList<String>();

    public AdapterNotificaciones(Context mContext,
                                 ArrayList titulo,
                                 ArrayList img,
                                 ArrayList detalle,
                                 ArrayList id) {
        this.mContext = mContext;
        this.titulo = titulo;
        this.img = img;
        this.id = id;
        this.detalle = detalle;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.item_notificaciones,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.titulo.setText(titulo.get(position));

        holder.detalle.setText(detalle.get(position));

        Glide.with(mContext).load(img.get(position)).into(holder.img);


        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                variables.id_principal = id.get(position);



            }
        });


    }

    @Override
    public int getItemCount() {
        return titulo.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView titulo;
        TextView detalle;
        ImageView img;

        CardView cardView ;

        public MyViewHolder(View itemView) {
            super(itemView);

            cardView =  itemView.findViewById(R.id.cardview_id);
            img = itemView.findViewById(R.id.book_img_id);
            titulo = itemView.findViewById(R.id.lbl_nombre_n);
            detalle = itemView.findViewById(R.id.lbl_detalle_n);

        }
    }



}
