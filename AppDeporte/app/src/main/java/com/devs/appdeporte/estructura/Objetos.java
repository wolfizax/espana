package com.devs.appdeporte.estructura;

import android.media.Image;
import android.service.controls.actions.FloatAction;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.solver.state.State;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class Objetos {

    public static ConstraintLayout
        home, notificacion, mensaje, spots, perfil, c_localizacion, c_deporte, c_deporte_spot, c_dificultad, c_actividad, c_fecha, c_duracion, c_spot, c_descripcion, c_crear_quedadas, c_crear_spot;

    public static TextView
        lbl_registro, lbl_titulo, lbl_localizacion, lbl_guardar, lbl_deporte, lbl_dificultad, lbl_actividad, lbl_fecha, lbl_duracion, lbl_spot, lbl_descripcion;

    public static EditText
            txt_descripcion;

    public static Button
        btn_login, btn_registrar;

    public static ImageView
        img_back;

    public static FloatingActionButton
        fab_post, fab_crear_quedada, fab_crear_spot;

    public static RecyclerView rv_quedadas, rv_spot, rv_notificaciones, rv_mensajes;

}
