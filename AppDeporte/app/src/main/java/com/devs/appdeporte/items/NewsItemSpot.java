package com.devs.appdeporte.items;

public class NewsItemSpot {


    String Title,Content,Date, UserPhoto;
    //int userPhoto;

    public NewsItemSpot() {
    }


    public NewsItemSpot(String title, String content, String date, String userPhoto) {
        Title = title;
        Content = content;
        Date = date;
        UserPhoto = userPhoto;
        //this.userPhoto = userPhoto;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public void setContent(String content) {
        Content = content;
    }

    public void setDate(String date) {
        Date = date;
    }

    public void setUserPhoto(String userPhoto) {
        UserPhoto = userPhoto;
    }

    public String getTitle() {
        return Title;
    }

    public String getContent() {
        return Content;
    }

    public String getDate() {
        return Date;
    }

    public String getUserPhoto() {
        return UserPhoto;
    }
}
