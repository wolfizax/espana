package com.devs.appdeporte.estructura;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.devs.appdeporte.items.AdapterDeportes;
import com.devs.appdeporte.items.AdapterMensajes;
import com.devs.appdeporte.items.AdapterNotificaciones;

import java.util.ArrayList;
import java.util.Calendar;

public class Funciones {

    AdapterNotificaciones adapterNotificaciones;
    AdapterMensajes adapterMensajes;

    Variables variables = new Variables();
    AdapterDeportes adapterDeportes;

    public void cargarvalores(Activity activity, RecyclerView myrv, ArrayList array_titulo, String modal){

        adapterDeportes = new AdapterDeportes(activity, array_titulo, modal);
        myrv.setLayoutManager(new GridLayoutManager(activity,1));
        myrv.setAdapter(adapterDeportes);

    }

    public void cargarNotificaciones(Activity activity, RecyclerView myrv, ArrayList array_titulo, ArrayList array_img, ArrayList array_id, ArrayList array_precio){

        adapterNotificaciones = new AdapterNotificaciones(activity, array_titulo, array_img, array_id, array_precio);
        myrv.setLayoutManager(new GridLayoutManager(activity,1));
        myrv.setAdapter(adapterNotificaciones);

    }

    public void cargarMensajes(Activity activity, RecyclerView myrv, ArrayList array_titulo, ArrayList array_img, ArrayList array_id, ArrayList array_precio){

        adapterMensajes = new AdapterMensajes(activity, array_titulo, array_img, array_id, array_precio);
        myrv.setLayoutManager(new GridLayoutManager(activity,1));
        myrv.setAdapter(adapterMensajes);

    }

    public void mensaje(Activity activity, String texto){

        Toast.makeText(activity,
                texto, Toast.LENGTH_LONG).show();

    }

    public void go(Activity activity, Class activity_go){

        Intent intent = new Intent(activity,activity_go);
        activity.startActivity(intent);

    }

    //Funcion para llamadas desde la app ------------
    public void makePhoneCall (Activity activity) {
        String number = variables.telefono;
        if (number.trim().length() > 0) {

            if (ContextCompat.checkSelfPermission(activity,
                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.CALL_PHONE}, variables.REQUEST_CALL);
            } else {
                String dial = "tel:" + number;
                activity.startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));

            }

        } else {
            Toast.makeText(activity, "Enter Phone Number", Toast.LENGTH_SHORT).show();
        }
    }

    public void NAVEGADOR(Activity activity, String link){

        Uri uri = Uri.parse(link);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        activity.startActivity(intent);

    }

    public void calendario(final TextView textView, Activity activity){

        final Calendar c= Calendar.getInstance();
        variables.dia =c.get(Calendar.DAY_OF_MONTH);
        variables.mes =c.get(Calendar.MONTH);
        variables.ano =c.get(Calendar.YEAR);

        final DatePickerDialog datePickerDialog = new DatePickerDialog(activity, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                variables.day = Integer.toString(dayOfMonth);
                variables.month = Integer.toString(monthOfYear+1);
                variables.year = Integer.toString(year);

                textView.setText(variables.day + "/" + variables.month + "/" + variables.year);

            }

        }
                ,variables.ano,variables.mes,variables.dia);

        datePickerDialog.show();

    }

    public void Deportes(){
        variables.array_deporte_nombre.add("escalada deportiva");
        variables.array_deporte_nombre.add("escalada alpina");
        variables.array_deporte_nombre.add("boulder");
        variables.array_deporte_nombre.add("psicoblock ");
        variables.array_deporte_nombre.add("escalada en hielo");
        variables.array_deporte_nombre.add("trekking");
        variables.array_deporte_nombre.add("alpinismo ");
        variables.array_deporte_nombre.add("senderismo");
        variables.array_deporte_nombre.add("SKI");
        variables.array_deporte_nombre.add("SNOW");
        variables.array_deporte_nombre.add("RAQUETAS DE NIEVE");
        variables.array_deporte_nombre.add("CICLISMO");
        variables.array_deporte_nombre.add("carretera");
        variables.array_deporte_nombre.add("mtb");
        variables.array_deporte_nombre.add("descenso");
        variables.array_deporte_nombre.add("bmx");
        variables.array_deporte_nombre.add("skate");
        variables.array_deporte_nombre.add("longboard");
        variables.array_deporte_nombre.add("PARKOUR");
        variables.array_deporte_nombre.add("SLACK LINE");
        variables.array_deporte_nombre.add("PAINTBALL");
        variables.array_deporte_nombre.add("AIRSOFT");
        variables.array_deporte_nombre.add("MOTO ");
        variables.array_deporte_nombre.add("motocross");
        variables.array_deporte_nombre.add("enduro");
        variables.array_deporte_nombre.add("trial");
        variables.array_deporte_nombre.add("QUAD");
        variables.array_deporte_nombre.add("ATV");
        variables.array_deporte_nombre.add("deportivo");
        variables.array_deporte_nombre.add("MOTO DE NIEVE");
        variables.array_deporte_nombre.add("SURF");
        variables.array_deporte_nombre.add("BODY BOARD");
        variables.array_deporte_nombre.add("PADDLESURF");
        variables.array_deporte_nombre.add("PIRAGÜSMO");
        variables.array_deporte_nombre.add("KAYAK");
        variables.array_deporte_nombre.add("RAFTING");
        variables.array_deporte_nombre.add("BUCEO");
        variables.array_deporte_nombre.add("SUBMARINISMO");
        variables.array_deporte_nombre.add("SNORKEL");
        variables.array_deporte_nombre.add("SKI ACUATICO");
        variables.array_deporte_nombre.add("WAKE BOARD");
        variables.array_deporte_nombre.add("DESCENSO DE BARRANCOS");
        variables.array_deporte_nombre.add("KITESURF");
        variables.array_deporte_nombre.add("WINDSURF");
        variables.array_deporte_nombre.add("MOTO DE AGUA");
        variables.array_deporte_nombre.add("PARACAIDISMO");
        variables.array_deporte_nombre.add("PARAPANTE");
        variables.array_deporte_nombre.add("SALTO BASE");
        variables.array_deporte_nombre.add("PUENTING");
        variables.array_deporte_nombre.add("WINGSUIT");



    }

    public void Dificultad(){

        variables.array_dificultad_nombre.add("1");
        variables.array_dificultad_nombre.add("2");
        variables.array_dificultad_nombre.add("3");
        variables.array_dificultad_nombre.add("4");
        variables.array_dificultad_nombre.add("5");
    }

    public void Actividad(){

        variables.array_actividad_nombre.add("Casual");
        variables.array_actividad_nombre.add("Entrenamiento");
        variables.array_actividad_nombre.add("Competicion");
        variables.array_actividad_nombre.add("Guiada");

    }

    public void Duracion(){

        variables.array_duracion_nombre.add("30 Minutos");
        variables.array_duracion_nombre.add("1 Hora");
        variables.array_duracion_nombre.add("1 Hora y 30 Minutos");
        variables.array_duracion_nombre.add("2 Horas");
        variables.array_duracion_nombre.add("2 Hora y 30 Minutos");
        variables.array_duracion_nombre.add("3 Horas");
        variables.array_duracion_nombre.add("3 Hora y 30 Minutos");
        variables.array_duracion_nombre.add("4 Horas");
        variables.array_duracion_nombre.add("4 Hora y 30 Minutos");
        variables.array_duracion_nombre.add("5 Horas");
        variables.array_duracion_nombre.add("5 Hora y 30 Minutos");
        variables.array_duracion_nombre.add("6 Horas");
        variables.array_duracion_nombre.add("6 Hora y 30 Minutos");
        variables.array_duracion_nombre.add("7 Horas");
        variables.array_duracion_nombre.add("7 Hora y 30 Minutos");
        variables.array_duracion_nombre.add("8 Horas");

    }



}
