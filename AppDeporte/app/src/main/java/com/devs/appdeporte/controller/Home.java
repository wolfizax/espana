 package com.devs.appdeporte.controller;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.devs.appdeporte.R;
import com.devs.appdeporte.estructura.Funciones;
import com.devs.appdeporte.estructura.Modals;
import com.devs.appdeporte.estructura.Objetos;
import com.devs.appdeporte.estructura.Variables;
import com.devs.appdeporte.items.NewsAdapter;
import com.devs.appdeporte.items.NewsAdapterSpot;
import com.devs.appdeporte.items.NewsItem;
import com.devs.appdeporte.items.NewsItemSpot;
import com.etebarian.meowbottomnavigation.MeowBottomNavigation;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class Home extends AppCompatActivity {

    NewsAdapter newsAdapter;
    NewsAdapterSpot newsAdapterSpot;
    List<NewsItem> mData;
    List<NewsItemSpot> mDataSpot;
    FloatingActionButton fabSwitcher;
    boolean isDark = false;
    ConstraintLayout rootLayout;
    EditText searchInput ;
    CharSequence search="";

    //----------

    Variables v = new Variables();
    Objetos o = new Objetos();
    Modals m = new Modals();
    Funciones funciones = new Funciones();
    Modals modals = new Modals();

    TextView selectPage;
    MeowBottomNavigation bottomNavigation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //------

        OBJETOS();

        cargar_lista();

        ACCIONES();

        //------

    }

    private void OBJETOS(){

        fabSwitcher = findViewById(R.id.fab_switcher);
        rootLayout = findViewById(R.id.root_layout);
        searchInput = findViewById(R.id.search_input);
        o.rv_quedadas = findViewById(R.id.news_rv);
        o.rv_spot = findViewById(R.id.rv_spot);
        o.rv_notificaciones = findViewById(R.id.rv_notificaciones);
        o.rv_mensajes = findViewById(R.id.rv_mensajes);

        o.lbl_deporte = findViewById(R.id.lbl_deporte);
        o.lbl_dificultad = findViewById(R.id.lbl_dificultad);
        o.lbl_actividad = findViewById(R.id.lbl_actividad);
        o.lbl_fecha = findViewById(R.id.lbl_fecha_quedada);
        o.lbl_duracion = findViewById(R.id.lbl_duracion);
        o.lbl_descripcion = findViewById(R.id.lbl_descripcion);
        o.lbl_localizacion = findViewById(R.id.lbl_spot_locaclizacion);

        o.home = findViewById(R.id.c_home);
        o.notificacion = findViewById(R.id.c_notificacion);
        o.spots = findViewById(R.id.c_spots);
        o.perfil = findViewById(R.id.c_perfil);
        o.mensaje = findViewById(R.id.c_mensaje);
        o.c_crear_quedadas = findViewById(R.id.c_crear_quedadas);
        o.c_deporte = findViewById(R.id.c_deporte);
        o.c_dificultad = findViewById(R.id.c_dificultad);
        o.c_actividad = findViewById(R.id.c_actividad);
        o.c_fecha = findViewById(R.id.c_fecha);
        o.c_duracion = findViewById(R.id.c_duracion);
        o.c_spot = findViewById(R.id.c_spot);
        o.c_crear_spot = findViewById(R.id.c_crear_spot);
        o.c_descripcion = findViewById(R.id.c_descripcion);
        o.c_localizacion = findViewById(R.id.c_localizacion);
        o.c_deporte_spot = findViewById(R.id.c_deporte_spot);

        o.lbl_titulo = findViewById(R.id.lbl_titulo);

        o.fab_post = findViewById(R.id.fab_post);
        o.fab_crear_quedada = findViewById(R.id.fab_crear_quedada);
        o.fab_crear_spot = findViewById(R.id.fab_crear_spot);

        selectPage = findViewById(R.id.select_page);
        bottomNavigation = findViewById(R.id.bottonNavigation);

        bottomNavigation.add(new MeowBottomNavigation.Model(v.ID_HOME, R.drawable.ic_home));
        bottomNavigation.add(new MeowBottomNavigation.Model(v.ID_SPOT, R.drawable.ic_location));
        bottomNavigation.add(new MeowBottomNavigation.Model(v.ID_NOTIFICATION, R.drawable.ic_notifications));
        bottomNavigation.add(new MeowBottomNavigation.Model(v.ID_MESSAGE, R.drawable.ic_baseline_comment_24));
        bottomNavigation.add(new MeowBottomNavigation.Model(v.ID_ACOUNT, R.drawable.ic_circle));

    }

    private void ACCIONES(){




        o.c_deporte_spot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    modals.modal_select_deporte(Home.this, "Hola");
            }
        });

        o.c_localizacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                funciones.go(Home.this, MapsActivity.class);
            }
        });

        o.fab_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                o.lbl_titulo.setText("Crear Quedadas");
                o.fab_post.setVisibility(View.GONE);
                o.home.setVisibility(View.GONE);
                o.mensaje.setVisibility(View.GONE);
                o.notificacion.setVisibility(View.GONE);
                o.spots.setVisibility(View.GONE);
                o.perfil.setVisibility(View.GONE);
                o.c_crear_spot.setVisibility(View.GONE);

                if (v.q_s.equals("1")){
                    o.c_crear_quedadas.setVisibility(View.VISIBLE);
                    o.fab_crear_quedada.setVisibility(View.VISIBLE);
                }else if(v.q_s.equals("2")){
                    o.lbl_titulo.setText("Crear Spot");
                    o.fab_post.setVisibility(View.GONE);
                    o.home.setVisibility(View.GONE);
                    o.mensaje.setVisibility(View.GONE);
                    o.notificacion.setVisibility(View.GONE);
                    o.spots.setVisibility(View.GONE);
                    o.perfil.setVisibility(View.GONE);
                    o.c_crear_spot.setVisibility(View.VISIBLE);
                    o.fab_crear_spot.setVisibility(View.VISIBLE);
                }

            }
        });

        bottomNavigation.setOnClickMenuListener(new MeowBottomNavigation.ClickListener() {
            @Override
            public void onClickItem(MeowBottomNavigation.Model item) {
                //Toast.makeText(Home.this, "Aca esta: " + item.getId(), Toast.LENGTH_SHORT).show();
            }
        });

        bottomNavigation.setOnShowListener(new MeowBottomNavigation.ShowListener() {
            @Override
            public void onShowItem(MeowBottomNavigation.Model item) {
                String name;

                switch (item.getId()){
                    case Variables.ID_HOME:
                        name = "Home";
                        o.home.setVisibility(View.VISIBLE);
                        o.mensaje.setVisibility(View.GONE);
                        o.notificacion.setVisibility(View.GONE);
                        o.spots.setVisibility(View.GONE);
                        o.perfil.setVisibility(View.GONE);
                        o.c_crear_spot.setVisibility(View.GONE);
                        o.fab_post.setVisibility(View.VISIBLE);
                        o.c_crear_quedadas.setVisibility(View.GONE);
                        o.fab_crear_quedada.setVisibility(View.GONE);
                        o.fab_post.setVisibility(View.VISIBLE);
                        v.q_s = "1";
                        o.lbl_titulo.setText("Home");

                        break;

                    case Variables.ID_MESSAGE:
                        name = "Message";
                        o.mensaje.setVisibility(View.VISIBLE);
                        o.home.setVisibility(View.GONE);
                        o.notificacion.setVisibility(View.GONE);
                        o.spots.setVisibility(View.GONE);
                        o.perfil.setVisibility(View.GONE);
                        o.fab_post.setVisibility(View.VISIBLE);
                        o.c_crear_quedadas.setVisibility(View.GONE);
                        o.fab_crear_quedada.setVisibility(View.GONE);
                        o.fab_post.setVisibility(View.GONE);
                        o.c_crear_spot.setVisibility(View.GONE);
                        bottomNavigation.setCount(v.ID_MESSAGE, "0");
                        o.lbl_titulo.setText("Mensajes");
                        break;

                    case Variables.ID_NOTIFICATION:
                        name = "Notification";
                        o.home.setVisibility(View.GONE);
                        o.mensaje.setVisibility(View.GONE);
                        o.notificacion.setVisibility(View.VISIBLE);
                        o.spots.setVisibility(View.GONE);
                        o.perfil.setVisibility(View.GONE);
                        o.fab_post.setVisibility(View.VISIBLE);
                        o.c_crear_quedadas.setVisibility(View.GONE);
                        o.fab_crear_quedada.setVisibility(View.GONE);
                        o.fab_post.setVisibility(View.GONE);
                        o.c_crear_spot.setVisibility(View.GONE);
                        bottomNavigation.setCount(v.ID_NOTIFICATION, "0");
                        o.lbl_titulo.setText("Notificaciones");
                        break;

                    case Variables.ID_ACOUNT:
                        name = "Account";
                        o.home.setVisibility(View.GONE);
                        o.mensaje.setVisibility(View.GONE);
                        o.notificacion.setVisibility(View.GONE);
                        o.spots.setVisibility(View.GONE);
                        o.perfil.setVisibility(View.VISIBLE);
                        o.fab_post.setVisibility(View.VISIBLE);
                        o.c_crear_quedadas.setVisibility(View.GONE);
                        o.fab_crear_quedada.setVisibility(View.GONE);
                        o.fab_post.setVisibility(View.GONE);
                        o.c_crear_spot.setVisibility(View.GONE);
                        o.lbl_titulo.setText("Perfil");
                        break;

                    case Variables.ID_SPOT:
                        name = "Spot";
                        o.home.setVisibility(View.GONE);
                        o.mensaje.setVisibility(View.GONE);
                        o.notificacion.setVisibility(View.GONE);
                        o.spots.setVisibility(View.VISIBLE);
                        o.perfil.setVisibility(View.GONE);
                        o.fab_post.setVisibility(View.VISIBLE);
                        o.c_crear_quedadas.setVisibility(View.GONE);
                        o.fab_crear_quedada.setVisibility(View.GONE);
                        o.fab_post.setVisibility(View.VISIBLE);
                        o.c_crear_spot.setVisibility(View.GONE);
                        v.q_s = "2";
                        o.lbl_titulo.setText("Spot");
                        break;

                    default: name = "";
                }
                selectPage.setText(getString(R.string.main_page_select, name));
            }
        });

        bottomNavigation.setOnReselectListener(new MeowBottomNavigation.ReselectListener() {
            @Override
            public void onReselectItem(MeowBottomNavigation.Model item) {
                //Toast.makeText(Home.this, "reselected item : " + item.getId(), Toast.LENGTH_SHORT).show();
                //o.home.setVisibility(View.VISIBLE);
                o.fab_post.setVisibility(View.VISIBLE);
                o.c_crear_quedadas.setVisibility(View.GONE);
                o.fab_crear_quedada.setVisibility(View.GONE);

            }
        });

        bottomNavigation.setCount(v.ID_NOTIFICATION, "4");
        bottomNavigation.setCount(v.ID_MESSAGE, "2");
        bottomNavigation.show(v.ID_HOME, true);

        //---------Crear Quedada------------------

        o.c_deporte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modals.modal_select_deporte(Home.this, "Hola");
            }
        });

        o.c_dificultad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modals.modal_select_dificultad(Home.this, "Hola");
            }
        });

        o.c_actividad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modals.modal_select_actividad(Home.this, "Hola");
            }
        });

        o.c_fecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                funciones.calendario(o.lbl_fecha, Home.this);
            }
        });

        o.c_duracion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modals.modal_select_duracion(Home.this, "Hola");
            }
        });

        o.c_spot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                funciones.mensaje(Home.this, "Spot");
            }
        });

        o.c_descripcion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modals.modal_descripcion(Home.this, "hola");
            }
        });

        o.fab_crear_quedada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                o.fab_post.setVisibility(View.VISIBLE);
                o.home.setVisibility(View.VISIBLE);
                o.c_crear_quedadas.setVisibility(View.GONE);
                o.fab_crear_quedada.setVisibility(View.GONE);
                o.lbl_titulo.setText("Home");
            }
        });

    }

    //------------

    private void saveThemeStatePref(boolean isDark) {

        SharedPreferences pref = getApplicationContext().getSharedPreferences("myPref",MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean("isDark",isDark);
        editor.commit();
    }

    private boolean getThemeStatePref () {

        SharedPreferences pref = getApplicationContext().getSharedPreferences("myPref",MODE_PRIVATE);
        boolean isDark = pref.getBoolean("isDark",false) ;
        return isDark;

    }

    private void cargar_lista(){

        funciones.Deportes();

        funciones.Dificultad();

        funciones.Actividad();

        funciones.Duracion();

        mData = new ArrayList<>();

        mDataSpot = new ArrayList<>();

        // load theme state

        isDark = getThemeStatePref();
        if(isDark) {
            // dark theme is on

            searchInput.setBackgroundResource(R.drawable.search_input_dark_style);
            rootLayout.setBackgroundColor(getResources().getColor(R.color.black));

        }
        else
        {
            // light theme is on
            searchInput.setBackgroundResource(R.drawable.search_input_style);
            rootLayout.setBackgroundColor(getResources().getColor(R.color.white));

        }

        // fill list news with data
        // just for testing purpose i will fill the news list with random data
        // you may get your data from an api / firebase or sqlite database ...
        mData.add(new NewsItem("Cantabria / Naranjo del Bulnes:","Lorem ipsum dolor sit amet, consectetur adipiscing elit","6 july 1994","https://blogs.forumsport.com/montana/wp-content/uploads/sites/3/2019/01/hitos_portada_2-1024x576.jpg"));
        mData.add(new NewsItem("I love Programming And Design","Lorem ipsum dolor sit amet, consectetur adipiscing elit","6 july 1994","https://cdn.pixabay.com/photo/2014/10/09/12/58/path-481745_960_720.jpg"));
        mData.add(new NewsItem("My first trip to Thailand story ","Lorem ipsum dolor sit amet, consectetur adipiscing elit","6 july 1994","https://static3.depositphotos.com/1003348/190/i/950/depositphotos_1904080-stock-photo-mountain-road.jpg"));
        mData.add(new NewsItem("After Facebook Messenger, Viber now gets...","Lorem Ipsum is simply dummy text of the printing and typesetting industry.","6 july 1994","https://img.lovepik.com/element/40114/9173.png_860.png"));
        mData.add(new NewsItem("Isometric Design Grid Concept","lorem ipsum dolor sit lorem ipsum dolor sit lorem ipsum dolor sit lorem ipsum dolor sit","6 july 1994","https://png.pngtree.com/png-vector/20201128/ourlarge/pngtree-grassland-png-image_2422637.jpg"));
        mData.add(new NewsItem("Android R Design Concept 4K","lorem ipsum dolor sit lorem ipsum dolor sit lorem ipsum dolor sit lorem ipsum","6 july 1994","https://ichef.bbci.co.uk/news/640/amz/worldservice/live/assets/images/2014/12/05/141205094439_bolivia_road-2.jpg"));
        mData.add(new NewsItem("OnePlus 6T Camera Review:","Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt","6 july 1994","https://www.boletomachupicchu.com/gutblt/wp-content/images/montana-machu-picchu.jpg"));
        mData.add(new NewsItem("I love Programming And Design","Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt","6 july 1994","https://c.wallhere.com/photos/a5/7b/5120x2880_px_clouds_Colorado_grass_landscape_Maroon_Bells_mountain_path-681010.jpg!d"));
        mData.add(new NewsItem("My first trip to Thailand story ","Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor","6 july 1994","https://www.desnivel.com/images/2016/10/el-camino-natural-del-port-de-vielha-esta-bien-senalizado-pero-nunca-esta-de-mas-llevar-un-buen-mapa.jpg"));
        mData.add(new NewsItem("After Facebook Messenger, Viber now gets...","Lorem Ipsum is simply dummy text of the printing and typesetting industry.","6 july 1994","https://okdiario.com/img/2018/05/17/como-orientarse-en-la-montana-655x368.jpg"));
        mData.add(new NewsItem("Isometric Design Grid Concept","lorem ipsum dolor sit lorem ipsum dolor sit lorem ipsum dolor sit lorem ipsum dolor sit","6 july 1994","https://www.lugaresdenieve.com/sites/default/files/muntania_0.jpg"));
        mData.add(new NewsItem("Android R Design Concept 4K","lorem ipsum dolor sit lorem ipsum dolor sit lorem ipsum dolor sit lorem ipsum dolor","6 july 1994","https://www.desnivel.com/images/2016/10/senal-vielha.jpg"));
        mData.add(new NewsItem("OnePlus 6T Camera Review:","Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt","6 july 1994","https://www.tiendasdecuadros.com/archivos/images/bpx0363-1573128118.jpg"));
        mData.add(new NewsItem("I love Programming And Design","Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt","6 july 1994","https://static.vecteezy.com/system/resources/previews/001/248/156/non_2x/rocky-pathway-to-mountain-free-photo.jpeg"));
        mData.add(new NewsItem("My first trip to Thailand story ","Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt","6 july 1994","https://media-cdn.tripadvisor.com/media/photo-s/16/66/15/38/montanas-de-los-7-colores.jpg"));
        mData.add(new NewsItem("After Facebook Messenger, Viber now gets...","Lorem Ipsum is simply dummy text of the printing and typesetting industry.","6 july 1994","https://cdn.pixabay.com/photo/2020/01/26/10/15/switzerland-4794236_960_720.jpg"));
        mData.add(new NewsItem("Isometric Design Grid Concept","lorem ipsum dolor sit lorem ipsum dolor sit lorem ipsum dolor sit lorem ipsum dolor sit","6 july 1994","https://c.wallhere.com/photos/8d/db/england_clouds_landscape_shadows_sheep_path_derbyshire_hill-512941.jpg!d"));
        mData.add(new NewsItem("Android R Design Concept 4K","lorem ipsum dolor sit lorem ipsum dolor sit lorem ipsum dolor sit lorem ipsum dolor sit lorem","6 july 1994","https://media.tacdn.com/media/attractions-splice-spp-674x446/07/c6/d1/25.jpg"));

        mDataSpot.add(new NewsItemSpot("Cantabria / Naranjo del Bulnes:","Lorem ipsum dolor sit amet, consectetur adipiscing elit","6 july 1994","https://blogs.forumsport.com/montana/wp-content/uploads/sites/3/2019/01/hitos_portada_2-1024x576.jpg"));
        mDataSpot.add(new NewsItemSpot("I love Programming And Design","Lorem ipsum dolor sit amet, consectetur adipiscing elit","6 july 1994","https://cdn.pixabay.com/photo/2014/10/09/12/58/path-481745_960_720.jpg"));
        mDataSpot.add(new NewsItemSpot("My first trip to Thailand story ","Lorem ipsum dolor sit amet, consectetur adipiscing elit","6 july 1994","https://static3.depositphotos.com/1003348/190/i/950/depositphotos_1904080-stock-photo-mountain-road.jpg"));
        mDataSpot.add(new NewsItemSpot("After Facebook Messenger, Viber now gets...","Lorem Ipsum is simply dummy text of the printing and typesetting industry.","6 july 1994","https://img.lovepik.com/element/40114/9173.png_860.png"));
        mDataSpot.add(new NewsItemSpot("Isometric Design Grid Concept","lorem ipsum dolor sit lorem ipsum dolor sit lorem ipsum dolor sit lorem ipsum dolor sit","6 july 1994","https://png.pngtree.com/png-vector/20201128/ourlarge/pngtree-grassland-png-image_2422637.jpg"));
        mDataSpot.add(new NewsItemSpot("Android R Design Concept 4K","lorem ipsum dolor sit lorem ipsum dolor sit lorem ipsum dolor sit lorem ipsum","6 july 1994","https://ichef.bbci.co.uk/news/640/amz/worldservice/live/assets/images/2014/12/05/141205094439_bolivia_road-2.jpg"));
        mDataSpot.add(new NewsItemSpot("OnePlus 6T Camera Review:","Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt","6 july 1994","https://www.boletomachupicchu.com/gutblt/wp-content/images/montana-machu-picchu.jpg"));
        mDataSpot.add(new NewsItemSpot("I love Programming And Design","Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt","6 july 1994","https://c.wallhere.com/photos/a5/7b/5120x2880_px_clouds_Colorado_grass_landscape_Maroon_Bells_mountain_path-681010.jpg!d"));
        mDataSpot.add(new NewsItemSpot("My first trip to Thailand story ","Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor","6 july 1994","https://www.desnivel.com/images/2016/10/el-camino-natural-del-port-de-vielha-esta-bien-senalizado-pero-nunca-esta-de-mas-llevar-un-buen-mapa.jpg"));
        mDataSpot.add(new NewsItemSpot("After Facebook Messenger, Viber now gets...","Lorem Ipsum is simply dummy text of the printing and typesetting industry.","6 july 1994","https://okdiario.com/img/2018/05/17/como-orientarse-en-la-montana-655x368.jpg"));
        mDataSpot.add(new NewsItemSpot("Isometric Design Grid Concept","lorem ipsum dolor sit lorem ipsum dolor sit lorem ipsum dolor sit lorem ipsum dolor sit","6 july 1994","https://www.lugaresdenieve.com/sites/default/files/muntania_0.jpg"));
        mDataSpot.add(new NewsItemSpot("Android R Design Concept 4K","lorem ipsum dolor sit lorem ipsum dolor sit lorem ipsum dolor sit lorem ipsum dolor","6 july 1994","https://www.desnivel.com/images/2016/10/senal-vielha.jpg"));
        mDataSpot.add(new NewsItemSpot("OnePlus 6T Camera Review:","Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt","6 july 1994","https://www.tiendasdecuadros.com/archivos/images/bpx0363-1573128118.jpg"));
        mDataSpot.add(new NewsItemSpot("I love Programming And Design","Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt","6 july 1994","https://static.vecteezy.com/system/resources/previews/001/248/156/non_2x/rocky-pathway-to-mountain-free-photo.jpeg"));
        mDataSpot.add(new NewsItemSpot("My first trip to Thailand story ","Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt","6 july 1994","https://media-cdn.tripadvisor.com/media/photo-s/16/66/15/38/montanas-de-los-7-colores.jpg"));
        mDataSpot.add(new NewsItemSpot("After Facebook Messenger, Viber now gets...","Lorem Ipsum is simply dummy text of the printing and typesetting industry.","6 july 1994","https://cdn.pixabay.com/photo/2020/01/26/10/15/switzerland-4794236_960_720.jpg"));
        mDataSpot.add(new NewsItemSpot("Isometric Design Grid Concept","lorem ipsum dolor sit lorem ipsum dolor sit lorem ipsum dolor sit lorem ipsum dolor sit","6 july 1994","https://c.wallhere.com/photos/8d/db/england_clouds_landscape_shadows_sheep_path_derbyshire_hill-512941.jpg!d"));
        mDataSpot.add(new NewsItemSpot("Android R Design Concept 4K","lorem ipsum dolor sit lorem ipsum dolor sit lorem ipsum dolor sit lorem ipsum dolor sit lorem","6 july 1994","https://media.tacdn.com/media/attractions-splice-spp-674x446/07/c6/d1/25.jpg"));

        // adapter ini and setup

        newsAdapter = new NewsAdapter(this,mData,isDark);
        o.rv_quedadas.setAdapter(newsAdapter);
        o.rv_quedadas.setLayoutManager(new LinearLayoutManager(this));

        newsAdapterSpot = new NewsAdapterSpot(this,mData,isDark);
        o.rv_spot.setAdapter(newsAdapterSpot);
        o.rv_spot.setLayoutManager(new LinearLayoutManager(this));


        fabSwitcher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isDark = !isDark ;
                if (isDark) {

                    rootLayout.setBackgroundColor(getResources().getColor(R.color.black));
                    searchInput.setBackgroundResource(R.drawable.search_input_dark_style);

                }
                else {
                    rootLayout.setBackgroundColor(getResources().getColor(R.color.white));
                    searchInput.setBackgroundResource(R.drawable.search_input_style);
                }

                newsAdapter = new NewsAdapter(getApplicationContext(),mData,isDark);
                if (!search.toString().isEmpty()){

                    newsAdapter.getFilter().filter(search);

                }
                o.rv_quedadas.setAdapter(newsAdapter);
                saveThemeStatePref(isDark);


            }
        });

        searchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                newsAdapter.getFilter().filter(s);
                search = s;


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        NOTIFICACIONES();

    }

    private void NOTIFICACIONES(){

        v.array_productos_id.clear();
        v.array_productos_titulo.clear();
        v.array_productos_precio.clear();
        v.array_productos_img.clear();

        v.array_productos_id.add("1");
        v.array_productos_id.add("2");
        v.array_productos_id.add("3");
        v.array_productos_id.add("4");
        v.array_productos_id.add("5");
        //variables.array_productos_id.add("6");

        v.array_productos_titulo.add("Sandra Martinez");
        v.array_productos_titulo.add("Rebeca Hernandez");
        v.array_productos_titulo.add("Oscar Briceño");
        v.array_productos_titulo.add("Daniela Alvarado");
        v.array_productos_titulo.add("Franklin Virgues");
        //variables.array_productos_titulo.add("Patacones");

        v.array_productos_precio.add("Tienes un nuevo seguidor");
        v.array_productos_precio.add("Comento una Quedada");
        v.array_productos_precio.add("Tienes un nuevo seguidor");
        v.array_productos_precio.add("Le dio me gusta a tu foto");
        v.array_productos_precio.add("Le dio me gusta a tu foto");
        //variables.array_productos_precio.add("85");

        v.array_productos_img.add("https://www.dzoom.org.es/wp-content/uploads/2020/02/portada-foto-perfil-redes-sociales-consejos.jpg");
        v.array_productos_img.add("https://socialtools.me/wp-content/uploads/2016/04/foto-de-perfil.jpg");
        v.array_productos_img.add("https://competenciasdelsiglo21.com/wp-content/uploads/2019/04/disc-perfil-c-azul-948x640.jpg");
        v.array_productos_img.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTj1KfaYOrwIrPVG_Yy3mNQgZ68_dXsawEi6w&usqp=CAU");
        v.array_productos_img.add("https://cdn.fastly.picmonkey.com/contentful/h6goo9gw1hh6/6NPSaM2xNL0vzz1mzz5Slt/efa79333c4fba9631cf4170c03693a39/1-intro-photo-final.jpg?w=800&q=70");


        funciones.cargarNotificaciones(Home.this, o.rv_notificaciones, v.array_productos_titulo, v.array_productos_img, v.array_productos_precio, v.array_productos_id);

        v.array_mensajes_id.clear();
        v.array_mensajes_titulo.clear();
        v.array_mensajes_precio.clear();
        v.array_mensajes_img.clear();

        v.array_mensajes_id.add("1");
        v.array_mensajes_id.add("2");
        v.array_mensajes_id.add("3");
        v.array_mensajes_id.add("4");
        v.array_mensajes_id.add("5");
        //variables.array_productos_id.add("6");

        v.array_mensajes_titulo.add("Oscar Briceño");
        v.array_mensajes_titulo.add("Sandra Martinez");
        v.array_mensajes_titulo.add("Rebeca Hernandez");
        v.array_mensajes_titulo.add("Daniela Alvarado");
        v.array_mensajes_titulo.add("Franklin Virgues");
        //variables.array_productos_titulo.add("Patacones");

        v.array_mensajes_precio.add("Excelente quedada amigo!");
        v.array_mensajes_precio.add("Muy buenos los recorridos que has realizado");
        v.array_mensajes_precio.add("Espero la proxima espedicion!");
        v.array_mensajes_precio.add("Suerte en la competencia");
        v.array_mensajes_precio.add("Cuando van de nuevo a surfear?");
        //variables.array_productos_precio.add("85");

        v.array_mensajes_img.add("https://competenciasdelsiglo21.com/wp-content/uploads/2019/04/disc-perfil-c-azul-948x640.jpg");
        v.array_mensajes_img.add("https://www.dzoom.org.es/wp-content/uploads/2020/02/portada-foto-perfil-redes-sociales-consejos.jpg");
        v.array_mensajes_img.add("https://socialtools.me/wp-content/uploads/2016/04/foto-de-perfil.jpg");
        v.array_mensajes_img.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTj1KfaYOrwIrPVG_Yy3mNQgZ68_dXsawEi6w&usqp=CAU");
        v.array_mensajes_img.add("https://cdn.fastly.picmonkey.com/contentful/h6goo9gw1hh6/6NPSaM2xNL0vzz1mzz5Slt/efa79333c4fba9631cf4170c03693a39/1-intro-photo-final.jpg?w=800&q=70");

        funciones.cargarMensajes(Home.this, o.rv_mensajes, v.array_mensajes_titulo, v.array_mensajes_img, v.array_mensajes_precio, v.array_mensajes_id);


    }


}