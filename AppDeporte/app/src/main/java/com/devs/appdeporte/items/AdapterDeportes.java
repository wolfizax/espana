package com.devs.appdeporte.items;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.devs.appdeporte.R;
import com.devs.appdeporte.estructura.Funciones;
import com.devs.appdeporte.estructura.Objetos;
import com.devs.appdeporte.estructura.Variables;

import java.util.ArrayList;

public class AdapterDeportes extends RecyclerView.Adapter<AdapterDeportes.MyViewHolder> {

    Variables variables = new Variables();

    Objetos objetos = new Objetos();

    Funciones funciones = new Funciones();

    String texto = "";

    boolean ischeckDone = false;

    private Context mContext ;
    ArrayList<String> nombre = new ArrayList<String>();
    String modal;

    String img_defauld = "https://3.bp.blogspot.com/-3_xUllAtoSU/VGkZhvhGL2I/AAAAAAABNII/rXZAXLozSR8/s1600/noticias.png";

    public AdapterDeportes(Context mContext, ArrayList nombre, String modal) {
        this.mContext = mContext;
        this.nombre = nombre;
        this.modal = modal;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.cardveiw_valores,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.titulo.setText(nombre.get(position));

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                texto = "Seleccionar " + nombre.get(position);
                objetos.lbl_guardar.setText(texto);

                if (modal.equals("1")){
                    objetos.lbl_deporte.setText(nombre.get(position));
                }else if(modal.equals("2")){
                    objetos.lbl_dificultad.setText(nombre.get(position));
                }else if(modal.equals("3")){
                    objetos.lbl_actividad.setText(nombre.get(position));
                }else if(modal.equals("4")){
                    objetos.lbl_duracion.setText(nombre.get(position));
                }


            }
        });



    }

    @Override
    public int getItemCount() {
        return nombre.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView titulo;

        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);

            cardView =  itemView.findViewById(R.id.cardview_id);
            titulo = itemView.findViewById(R.id.lbl_nombre);

        }
    }

}